package fr.ynovBank.javaBankDoux.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import fr.ynovBank.javaBankDoux.model.Client;

public class ClientManager {

	public static Client loadClientByID(int clientID) {
		Client result = new Client();
		
		try {
			Connection con = DriverManager
					.getConnection("jdbc:mysql://localhost:3306/applibanquejee","root","");
			
			//Statement stmt= con.createStatement();
			PreparedStatement stmt = con.prepareStatement("SELECT idClient, nom, prenom, login FROM client WHERE idClient=?");
			stmt.setInt(1, clientID);
			ResultSet rs = stmt.executeQuery();
			
			while (rs.next()) {
				result.setClientID(rs.getInt("idClient"));
				result.setNom(rs.getString("nom"));
				result.setPrenom(rs.getString("prenom"));
				result.setLogin(rs.getString("login"));
				
				System.out.println("Clients trouvés : "+result.toString());
			}
			
			rs.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
}
